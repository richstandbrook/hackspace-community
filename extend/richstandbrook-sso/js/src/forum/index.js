import {extend} from "flarum/extend";
import app from "flarum/app";
import SettingsPage from "flarum/components/SettingsPage";
import FieldSet from 'flarum/components/FieldSet';
import Button from 'flarum/components/Button'
import ItemList from 'flarum/utils/ItemList';

app.initializers.add('flarum-ext-account-links', function () {

  extend(SettingsPage.prototype, 'settingsItems', (items) => {

    const accountItems = new ItemList();
    accountItems.add('updateAccount', Button.component({
      children: 'Change email or password',
      className: 'Button',
      onclick: () => window.location = app.forum.data.attributes['maicol07-sso.login_url']
    }));

    items.add('account',
      FieldSet.component({
        label: app.translator.trans('core.forum.settings.account_heading'),
        className: 'Settings-account',
        children: accountItems.toArray()
      }),
      3
    );
  });


});
